import asyncio
import logging
import argparse
from time import time
from eventBusPy.lib.Eventbus import (Eventbus, DeliveryOption)


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

stdout_logger = logging.StreamHandler()
stdout_logger.setLevel(logging.DEBUG)
stdout_logger.setFormatter(logging.Formatter('%(asctime)s [%(levelname)s] %(module)s %(funcName)s %(message)s'))

logger.addHandler(stdout_logger)


class PyExecutorHandler(object):

    def __init__(self, eb):
        self.eb = eb

    def execute(self, message):
        if message and 'body' in message:
            python_code = message['body'].get('CODE', None)
            extracted_data = message['body'].get('EXTRACTED_DATA', None)

            if python_code is not None and extracted_data is not None:

                try:
                    logger.debug("Code to compile:")
                    logger.debug(python_code)

                    logger.debug(f"Extracted data {extracted_data}.")
                    python_code = compile(python_code, '<string>', 'exec')

                    global_ns = {
                        'EXTRACTED_DATA': extracted_data
                    }

                    local_ns = {}

                    logger.debug(f"Executing Python code {python_code}.")
                    exec(python_code, global_ns, local_ns)

                    global_ns.update(local_ns)
                    body = {'time': time(), "type": "result", "from": "pyExecutor", "result": local_ns['run']()}

                    do = DeliveryOption.DeliveryOption()

                    [do.addHeader(h, message.get('headers').get(h)) for h in message.get("headers", {}).keys()]
                    logger.debug(f"Replying with message {body} and options {do}.")

                    self.eb.send(message.get("replyAddress"), body, do)
                except Exception as ex:
                    logger.error("Problem with executing and replying code.")
                    logger.exception(ex)

                    do = DeliveryOption.DeliveryOption()

                    [do.addHeader(h, message.get('headers').get(h)) for h in message.get("headers", {}).keys()]
                    logger.debug(f"Replying with message {body} and options {do}.")
                    body = {'time': time(), "from": "pyExecutor", "type": "error", "result": f"{ex}"}

                    self.eb.send(message.get("replyAddress"), body, do)


if __name__ == "__main__":
    logger.debug("Starting PyExecutor event bus ...")

    parser = argparse.ArgumentParser("Vertx event bus processor for Python code execution.")
    parser.add_argument("--host", type=str, required=False, default="127.0.0.1")
    parser.add_argument("--port", type=int, required=False, default=7000)
    parser.add_argument("--address", type=str, required=False, default="pyExecutor")

    args = parser.parse_args()

    logger.debug("Creating event bus object ...")

    eb = Eventbus.Eventbus(host=args.host, port=args.port)

    loop = asyncio.get_event_loop()

    try:
        pyExecHandler = PyExecutorHandler(eb)
        eb.registerHandler(args.address, pyExecHandler.execute)
        loop.run_forever()
    except KeyboardInterrupt:
        logger.debug("Keyboard interrupt detected closing application ...")
    except Exception as ex:
        logger.exception(ex)
    finally:
        eb.closeConnection(2, close_app=True)
